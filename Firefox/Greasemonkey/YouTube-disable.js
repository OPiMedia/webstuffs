// ==UserScript==
// @name          YouTube-disable
// @description   Disable comments and propositions beside YouTube player.
// @include       http*://*.youtube.com/*
// @grant         none
// ==/UserScript==

/* jshint esversion: 6 */

/*
 * YouTube disable (October 25, 2020)
 *
 * Script for Greasemonkey plugin of Firefox:
 * https://addons.mozilla.org/fr/firefox/addon/greasemonkey/
 *
 * GPLv3 --- Copyright (C) 2020 Olivier Pirson --- https://www.gnu.org/licenses/gpl-3.0.html
 * Olivier Pirson --- http://www.opimedia.be/
 */

(function () {
    "use strict";

    const ids = ["comments", "secondary", "secondary-inner"];

    window.setInterval(function () {
        for (let id of ids) {
            const item = document.getElementById(id);

            if (item !== null) {
                item.style.display = "none";
            }
        }
    }, 2 * 1000);
}());
