// ==UserScript==
// @name          Social-delay
// @description   Mask Facebook, diaspora*, Twitter and Mastodon during 30 seconds.
// @include       https://www.facebook.com/*
// @include       https://diaspora-fr.org/*
// @include       https://twitter.com/*
// @include       https://mamot.fr/*
// @require       https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @grant         none
// ==/UserScript==

/* jshint esversion: 6 */

/*
 * Social delay (October 17, 2021)
 *
 * Script for Greasemonkey plugin of Firefox:
 * https://addons.mozilla.org/fr/firefox/addon/greasemonkey/
 *
 * GPLv3 --- Copyright (C) 2020-2021 Olivier Pirson --- https://www.gnu.org/licenses/gpl-3.0.html
 * Olivier Pirson --- http://www.opimedia.be/
 */

(function () {
    "use strict";

    const now = new Date(Date.now());

    console.log("Facebook delay START", now);
    if ((10 <= now.getHours()) && (now.getHours() <= 19)) {
        const body = document.getElementsByTagName("body")[0];

        body.style.opacity = 0.1;

        setTimeout(function () {
            body.style.opacity = 1;

            console.log("Facebook delay END");
        }, 30 * 1000);
    }
}());
