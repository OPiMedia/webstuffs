<?php /* -*- coding: utf-8 -*- */

/** \file pypi-get-stats.php
 *
 * \brief
 * PHP script to get statistics of a Python's package from pypi.python.org
 * and return it.
 *
 * Usage:
 *   pypi-get-stats.php?package-name=...[&type=...[&lang=...]]
 * Options:
 *   package-name: package name
 *   type: json|html-li (default: json)
 *   lang: en|fr (default: en)
 *
 * Example:
 *   pypi-get-stats.php?package-name=SimpleGUICS2Pygame&type=html-li&lang=fr
 * read statistics from SimpleGUICS2Pygame from
 *   https://pypi.python.org/pypi/SimpleGUICS2Pygame
 *   and return it in a string with (at most) 3 <li> HTML tags in French
 *   like here
 *   http://www.opimedia.be/CV/realisations.htm#SimpleGUICS2Pygame-stats
 *
 *
 *
 * GPL3 --- Copyright (C) 2016 Olivier Pirson
 * http://www.opimedia.be/
 * https://bitbucket.org/OPiMedia/webstuffs
 *
 * @version 01.01.00 --- April 15, 2016
 * @author Olivier Pirson <olivier.pirson.opi@gmail.com>
 *
 * GPLv3
 * ------
 * Copyright (C) 2016 Olivier Pirson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#DEBUG
if (true) {
  // Development configuration
  ini_set('display_errors', 'stdout');
  ini_set('display_startup_errors', 1);
  ini_set('html_errors', 1);

  error_reporting(-1);

  assert_options(ASSERT_ACTIVE, true);
  assert_options(ASSERT_WARNING, true);
  assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
  // Production configuration
  ini_set('display_errors', 'stderr');
  ini_set('display_startup_errors', 0);
  ini_set('html_errors', 0);

  error_reporting(0);

  assert_options(ASSERT_ACTIVE, false);
  assert_options(ASSERT_WARNING, false);
  assert_options(ASSERT_BAIL, false);
#DEBUG
}
#DEBUG_END


mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');



/*************
 * Functions *
 *************/
/**
 * \brief
 * Get statistics of the $package_name from pypi.python.org
 * and return it.
 *
 * @param string $package_name
 *
 * @return ['day' => int, 'week' => int, 'month' => int] or false
 */
function pypi_get_stats($package_name) {
#DEBUG
  assert(is_string($package_name));
#DEBUG_END

  // Load content
  $url = 'https://pypi.python.org/pypi/'.$package_name;
  $page = file_get_contents($url);

  // Build result
  $result = false;

  if (($page !== false)
      && preg_match('|<li>\s*<span>(\d+)</span>\s+downloads?\s+in\s+the\s+last\s+day\s*</li>\s*<li>\s*<span>(\d+)</span>\s+downloads?\s+in\s+the\s+last\s+week\s*</li>\s*<li>\s*<span>(\d+)</span>\s+downloads?\s+in\s+the\s+last\s+month\s*</li>|i',
                    $page, $matches)) {
    return ['day' => (int)$matches[1],
            'week' => (int)$matches[2],
            'month' => (int)$matches[3]];
  }
  else {
    return false;
  }
}


/**
 * \brief
 * Get statistics of the $package_name from pypi.python.org
 * and return it in a string.
 *
 * @param string $package_name
 * @param string $type
 * @param string $lang
 *
 * @return string or false
 */
function pypi_get_stats_string($package_name, $type='json', $lang='en') {
#DEBUG
  assert(is_string($package_name));
  assert(is_string($type));
  assert(is_string($lang));
#DEBUG_END

  $stats = pypi_get_stats($package_name);
  if ($stats === false) {
    return false;
  }

  if ($type === 'html-li') {
    $day = $stats['day'];
    $week = $stats['week'];
    $month = $stats['month'];

    $a = '';

    if ($lang === 'fr') {
      if ($day) {
        $a[] = '<li><span>'.$day.'</span> téléchargement'.s($day).' durant le dernier jour</li>';
      }
      if ($week && ($day < $week)) {
        $a[] = '<li><span>'.$week.'</span> téléchargement'.s($week).' durant la dernière semaine</li>';
      }
      if ($month && ($week < $month)) {
        $a[] = '<li><span>'.$month.'</span> téléchargement'.s($month).' durant le dernier mois</li>';
      }
    }
    else {
      if ($day) {
        $a[] = '<li><span>'.$day.'</span> download'.s($day).' in the last day</li>';
      }
      if ($week && ($day < $week)) {
        $a[] = '<li><span>'.$week.'</span> download'.s($week).' in the last week</li>';
      }
      if ($month && ($week < $month)) {
        $a[] = '<li><span>'.$month.'</span> download'.s($month).' in the last month</li>';
      }
    }

    return ($a
            ? join($a, '
')
            : '');
  }
  else {
    return json_encode($stats);
  }
}


/**
 * \brief
 * Return 's' then if $n >= 2,
 * else return ''.
 *
 * @param numeric $n
 *
 * @return string
 */
function s($n) {
#DEBUG
  assert(is_numeric($n));
#DEBUG_END

  return ($n >= 2
          ? 's'
          : '');
}



/********
 * Main *
 ********/
if (isset($_GET['package-name'])) {
  $result = pypi_get_stats_string($_GET['package-name'],
                                  (empty($_GET['type'])
                                   ? 'json'
                                   : $_GET['type']),
                                  (empty($_GET['lang'])
                                   ? 'en'
                                   : $_GET['lang']));

  if ($result !== false) {
    echo $result;
  }
  else {
    header('HTTP/1.0 404 Not Found');

    exit(1);
  }
}

?>