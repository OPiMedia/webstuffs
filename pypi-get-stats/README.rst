.. -*- restructuredtext -*-

==============
pypi-get-stats
==============

.. warning::
   This script is deprecated
   because `pypi.python.org`_ does not display more required informations in HTML pages.
   On the other hand you can get informations in the JSON format like this :
   https://pypi.python.org/pypi/SimpleGUICS2Pygame/json

   See PyPIJSON - Python Wiki: https://wiki.python.org/moin/PyPIJSON

PHP script to get statistics of a Python's package from `pypi.python.org`_ and return it.

.. _`pypi.python.org`: https://pypi.python.org/

|



Usage
=====
  ``pypi-get-stats.php?package-name=...[&type=...[&lang=...]]``

Parameters
----------
* ``package-name``: package name
* ``type``: ``json`` | ``html-li`` (default: ``json``)
* ``lang``: ``en`` | ``fr`` (default: ``en``)

Example
~~~~~~~
   ``pypi-get-stats.php?package-name=SimpleGUICS2Pygame&type=html-li&lang=fr``

read statistics from SimpleGUICS2Pygame from
https://pypi.python.org/pypi/SimpleGUICS2Pygame
and return it in a string with 3 (at most) <li> HTML tags in French,
like here
`www.opimedia.be/CV/realisations.htm#SimpleGUICS2Pygame-stats`_

.. _`www.opimedia.be/CV/realisations.htm#SimpleGUICS2Pygame-stats`: http://www.opimedia.be/CV/realisations.htm#SimpleGUICS2Pygame-stats

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



Licenses
========
All my personal contributions are under GPLv3.
Other stuff belong to their respective authors.

https://bitbucket.org/OPiMedia/webstuffs

GPLv3_ |GPLv3|
--------------
Copyright (C) 2016 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png

|



Changes
=======
* 01.01.00 — April 15, 2016

  - Don't add <li> with same number.

* 01.00.00 — January 8, 2016
