<?php /* -*- coding: utf-8 -*- */

/** \file Qwant-proxy.php
 *
 * \brief
 * Simple proxy to the web search engine Qwant.
 * Add possibility to a sitesearch parameter (like Google).
 *
 * Usage
 *   Qwant-proxy.php?[Qwant GET parameters or sitesearch=...]
 *
 * See https://www.qwant.com/
 * and https://www.qwant.com/faq
 *
 * It simply redirects to the
 *   https://www.qwant.com/?[Qwant GET parameters]
 *   but potential sitesearch=... are removed
 *   and added to a q=... parameter.
 *
 * For example,
 *   Qwant-proxy.php?q=Python%20OR%20Scala&sitesearch=www.opimedia.be&t=web
 * is redirected to
 *   https://www.qwant.com/?q=Python%20OR%20Scala%20site:www.opimedia.be&t=web
 *
 * By default only localhost and www.opimedia.be hosts are permit.
 *
 * I use it in my personal website http://www.opimedia.be/
 * to deal with 2 options of search.
 *
 * GPL3 --- Copyright (C) 2016 Olivier Pirson
 * http://www.opimedia.be/
 * https://bitbucket.org/OPiMedia/webstuffs
 *
 * @version 01.00.00 --- July 31, 2016
 * @author Olivier Pirson <olivier.pirson.opi@gmail.com>
 *
 * GPLv3
 * ------
 * Copyright (C) 2016 Olivier Pirson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');


#DEBUG
if (true) {
  // Development configuration
  ini_set('display_errors', 'stdout');
  ini_set('display_startup_errors', 1);
  ini_set('html_errors', 1);

  error_reporting(-1);

  assert_options(ASSERT_ACTIVE, true);
  assert_options(ASSERT_WARNING, true);
  assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
  // Production configuration
  ini_set('display_errors', 'stderr');
  ini_set('display_startup_errors', 0);
  ini_set('html_errors', 0);

  error_reporting(0);

  assert_options(ASSERT_ACTIVE, false);
  assert_options(ASSERT_WARNING, false);
  assert_options(ASSERT_BAIL, false);
#DEBUG
}
#DEBUG_END


mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');



/**********
 * Config *
 **********/
$AUTHORIZED_HOSTS = ['localhost', 'www.opimedia.be'];

$QWANT_URL = 'https://www.qwant.com/';



/********
 * Main *
 ********/
// Check if the host is permit.
if (!in_array($_SERVER['HTTP_HOST'], $AUTHORIZED_HOSTS, true)) {
    header('HTTP/1.0 404 Not Found');

    exit(1);
}

$params = [];
$sitesearch = null;

// Collect all params and exclude 'sitesearch' param
foreach ($_GET as $key=>$value) {
    if ($key !== rawurlencode($key)) {
        continue;
    }

    if ($key === 'sitesearch') {
        $sitesearch = rawurlencode($value);
    }
    else {
        $params[$key] = $key.'='.rawurlencode($value);
    }
}

// Add potential 'sitesearch' param to 'q' param
if (!empty($sitesearch)) {
    $params['q'] = (empty($params['q'])
                    ? 'site:'.$sitesearch
                    : $params['q'].'%20site:'.$sitesearch);
}

// Build URL
$url = $QWANT_URL;
$params = implode($params, '&');
if (!empty($params)) {
    $url .= '?'.$params;
}

// Go to Qwant page
header('Location: '.$url);

exit;

?>