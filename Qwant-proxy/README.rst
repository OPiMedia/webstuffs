.. -*- restructuredtext -*-

===========
Qwant-proxy
===========

| Simple proxy to the web search engine Qwant_.
| **Add** possibility to a **sitesearch parameter** (like Google).

.. _Qwant: https://www.qwant.com/

|



Usage
=====
``Qwant-proxy.php?[Qwant GET parameters or sitesearch=...]``

See https://www.qwant.com/faq

It simply redirects to the
  ``https://www.qwant.com/?[Qwant GET parameters]``
  but potential ``sitesearch=...`` are removed
  and added to a ``q=...`` parameter.

For example,
  ``Qwant-proxy.php?q=Python%20OR%20Scala&sitesearch=www.opimedia.be&t=web``
is redirected to
  https://www.qwant.com/?q=Python%20OR%20Scala%20site:www.opimedia.be&t=web

By default only localhost and ``www.opimedia.be`` hosts are permit.

I use it in my personal website http://www.opimedia.be/
to deal with 2 options of search.

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



Licenses
========
All my personal contributions are under GPLv3.
Other stuff belong to their respective authors.

https://bitbucket.org/OPiMedia/webstuffs

GPLv3_ |GPLv3|
--------------
Copyright (C) 2016 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png

|



Changes
=======
* 01.00.00 — July 31, 2016
