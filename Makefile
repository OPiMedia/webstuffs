# Makefile of WebStuffs --- 2014-07-03

.SUFFIXES:

CHECKTXT = checkTxtPy.py
SHELL    = sh



########
# Test #
########
.PHONY: checkTxt

checkTxt:
	@$(CHECKTXT) README.RST "*/*.html" "*/*css/*.css" "*/*js/*.js"
