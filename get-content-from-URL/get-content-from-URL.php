<?php /* -*- coding: utf-8 -*- */

/** \file get-content-from-URL.php
 *
 * \brief
 * Gets the content of the specified URL and returns it, without change.
 *
 * Usage
 *   get-content-from-URL.php?URL=...
 *
 * Parameters
 *   URL: valid URL, starting with 'http://' or 'https://'
 *
 *
 * GPL3 --- Copyright (C) 2016, 2020 Olivier Pirson
 * http://www.opimedia.be/
 * https://bitbucket.org/OPiMedia/webstuffs
 *
 * @version 01.00.02 --- June 17, 2020
 * @author Olivier Pirson <olivier.pirson.opi@gmail.com>
 *
 * GPLv3
 * ------
 * Copyright (C) 2016, 2020 Olivier Pirson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');


#DEBUG
if (true) {
    // Development configuration
    ini_set('display_errors', 'stdout');
    ini_set('display_startup_errors', 1);
    ini_set('html_errors', 1);

    error_reporting(-1);

    assert_options(ASSERT_ACTIVE, true);
    assert_options(ASSERT_WARNING, true);
    assert_options(ASSERT_BAIL, true);
}
else {
#DEBUG_END
    // Production configuration
    ini_set('display_errors', 'stderr');
    ini_set('display_startup_errors', 0);
    ini_set('html_errors', 0);

    error_reporting(0);

    assert_options(ASSERT_ACTIVE, false);
    assert_options(ASSERT_WARNING, false);
    assert_options(ASSERT_BAIL, false);
#DEBUG
}
#DEBUG_END


mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');



/********
 * Main *
 ********/
if (!empty($_GET['URL'])) {
    $url = $_GET['URL'];

    if (preg_match('|^https?://|', $url)) {
        set_time_limit(5);
        echo file_get_contents($url);

        exit;
    }
}

header('HTTP/1.0 404 Not Found');

exit(1);

?>