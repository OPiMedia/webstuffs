.. -*- restructuredtext -*-

====================
get-content-from-URL
====================

Gets the content of the specified URL and returns it, without change.


Usage
=====
  ``get-content-from-URL.php?URL=...``

Parameters
----------
* ``URL``: valid URL, starting with ``http://`` or ``https://``

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



Licenses
========
All my personal contributions are under GPLv3.
Other stuff belong to their respective authors.

https://bitbucket.org/OPiMedia/webstuffs

GPLv3_ |GPLv3|
--------------
Copyright (C) 2016, 2020 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.html

.. |GPLv3| image:: https://www.gnu.org/graphics/gplv3-88x31.png

|



Changes
=======
* 01.00.02 — June 17, 2020

  - Updated old email.

* 01.00.01 — September 3, 2016

  - Reindented code.

* 01.00.00 — March 5, 2016
