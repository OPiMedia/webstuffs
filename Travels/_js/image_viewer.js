/*
 * Travel (September 24, 2014)
 *
 * Piece of WebStuffs.
 * https://bitbucket.org/OPiMedia/webstuffs
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

/*
 * If filename is kind of '*_th.*'
 * then return '**',
 * else return filename.
 *
 * Pre: filename: String
 *
 * Return: String
 */
function filename_remove_th(filename) {
    'use strict';

    return (filename.substr(filename.length - 7, 4) === '_th.' ? filename.substr(0, filename.length - 7) + filename.substr(filename.length - 4) : filename);
}


/*
 * Hide the viewer.
 */
function image_viewer_off() {
    'use strict';

    document.getElementById('image_viewer').style.display = 'none';
}


/*
 * Show the image on the viewer.
 *
 * Pre: img: <img> HTML item
 */
function image_viewer_on(img) {
    'use strict';

    var div_image_viewer = document.getElementById('image_viewer');
    var div_image_viewer_img = div_image_viewer.children[0];

    div_image_viewer_img.src = filename_remove_th(img.src);

    div_image_viewer_img.style.width = (document.documentElement.clientWidth*80/100) + 'px';
    div_image_viewer_img.style.height = (document.documentElement.clientHeight*80/100) + 'px';

    div_image_viewer.style.display = 'flex';
}
