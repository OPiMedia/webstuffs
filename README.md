# WebStuffs

* [`Firefox/`](https://bitbucket.org/OPiMedia/webstuffs/src/master/Firefox/)

    - [userChrome.css](https://bitbucket.org/OPiMedia/webstuffs/src/master/Firefox/userChrome.css):
      Simple CSS file to customize the look of Mozilla\'s user interface.
    - [`Greasemonkey/`](https://bitbucket.org/OPiMedia/webstuffs/src/master/Firefox/Greasemonkey/)

        - [`Social-delay.js`](https://bitbucket.org/OPiMedia/webstuffs/src/master/Firefox/Greasemonkey/Social-delay.js):
          mask Facebook, diaspora*, Twitter and Mastodon during 30 seconds
        - [`YouTube-disable.js`](https://bitbucket.org/OPiMedia/webstuffs/src/master/Firefox/Greasemonkey/YouTube-disable.js):
          disable comments and propositions beside YouTube player

* [get-content-from-URL](https://bitbucket.org/OPiMedia/webstuffs/src/master/get-content-from-URL/):
  PHP script to **gets the content of the specified URL and returns it**
* [get-OPiMedia-bitbucket-PDF](https://bitbucket.org/OPiMedia/webstuffs/src/master/get-OPiMedia-bitbucket-PDF/):
  PHP script to **gets the content of the specified PDF file and returns it**
* [Qwant-proxy](https://bitbucket.org/OPiMedia/webstuffs/src/master/Qwant-proxy/):
  PHP simple **proxy** to the **web search engine** [Qwant](https://www.qwant.com/) **with sitesearch parameter**

* **Portfolio**: little fake website **HTML5/CSS3 exercise**.
  Online version:
  [Portfolio](http://www.opimedia.be/CV/2014_Evoliris/HTML5_et_CSS3_pour_applications_mobiles/Portfolio/index.html).
* **Travels**: little fake website **HTML5/CSS3 exercise**.
  Online version:
  [Travels](http://www.opimedia.be/CV/2014_Evoliris/HTML_5_et_CSS_3_pour_le_Web/Travels/index.html).



Other personal links
--------------------

* [CodePen](http://codepen.io/OPiMedia/)
* [Développement Web](http://www.opimedia.be/DS/webdev/) /
  [liens](http://www.opimedia.be/DS/webdev/liens.htm)



Deprecated
----------

* [pypi-get-stats](https://bitbucket.org/OPiMedia/webstuffs/src/master/pypi-get-stats/):
  PHP script to **get statistics of a Python\'s package** from [pypi.python.org](https://pypi.python.org/)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)

Copyright (C) 2014-2018, 2020-2021 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



## Changes
* October 20, 2020: Added Greasemonkey scripts `Social-delay.js` and `YouTube-disable.js`.
* June 13, 2020: `get-OPiMedia-bitbucket-PDF`: replaced Mercurial URL by Git URL.
* September 8, 2018: `userChrome.css`: added.
* September 9, 2017: `get-OPiMedia-bitbucket-PDF`: added.

* August 7, 2016

    - Portfolio and Travels: removed target="_blank".
    - Portfolio: added lang.

* July 31, 2016: Qwant-proxy: added.
* March 5, 2016: `get-content-from-URL`: added.
* January 8, 2016: `pypi-get-stats`: added.
* September 25, 2014: Portfolio: added.
* July 3, 2014: Travels: Improved JavaScript loading.
* May 9, 2014: Opened repository.
