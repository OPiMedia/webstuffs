/*
 * Portfolio
 *
 * portfolio.js (Septembre 25, 2014)
 *
 * Piece of WebStuffs.
 * https://bitbucket.org/OPiMedia/webstuffs
 *
 * GPLv3 --- Copyright (C) 2014 Olivier Pirson
 * http://www.opimedia.be/
 */

/*
 * Scroll to left top.
 */
function go_top() {
    'use strict';

    window.scrollTo(0, 0);
}


/*
 * Main
 */
if (window.addEventListener) {
    window.addEventListener('hashchange', go_top);
} else if (window.attachEvent) {  // IE <= 8
    window.attachEvent('hashchange', go_top);
}
